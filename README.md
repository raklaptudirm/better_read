# better_read

A better readline for Node.js with features like autofill and custom visuals.

# Installation

```bash
npm install better_read
```

# Usage

`better_read` works with promises, enabling both synchronous and asynchronous input taking.

Synchronous use:

```js
const read = require("better_read")

async fuction main() {
  console.log("\n" + await read.prompt("> ")
}

main()
```

Asynchronous use:

```js
const read = require("better_read")

read.prompt("> ").then(input => console.log("\n" + input)
```

# API:

## `prompt`

```js
read.prompt([query[, hide]])
```

| Type    | `async`     |
| ------- | ----------- |
| Returns | `Promise<>` |

Dispalys a `query` to the user and prompts them for input.

### Example:

```js
async function question(query) {
  console.log(await read.prompt(query))
}

question("Synchronous with async-await: ")
read.prompt("Asynchronous with then: ").then(input => console.log(input))
```

### `query`:

| Type     | `string` |
| -------- | -------- |
| Optional | `false`  |

Query to show to the user.

### `hide`

| Type     | `boolean` |
| -------- | --------- |
| Optional | `true`    |
| Default  | `false`   |

If true, the input is masked with the `*` character.

## `setVisual`

```js
read.setVisual([function])
```

| Type    | `sync` |
| ------- | ------ |
| Returns | `void` |

The visual effects (hints, previews, usage) for the next prompt is set with this function. It is reset automatically after calling `prompt`.

### Example:

This example automatically shows the length of the input string:

```js
async function main() {
  read.setVisual(input => console.log(input.length))
  let input = read.prompt("Length of: ")
  console.log("Length of " + input + " is " + input.length + ".")
}
main()
```

This function is automatically called when a change occurs to the input with a single argument, the input at the time. The function can freely print everything it wants according to the input argument. The printing automatically begins from right after the input:

```
> input_text<printing starts here>
```

The application automatically handles all the erasing and re-rendering, so you do not need to include any erasing functions.

### `function`

| Type     | `string` |
| -------- | -------- |
| Optional | `false`  |

The function to be called for visualization.

## `setAutofill`

```js
read.setAutofill([function])
```

| Type    | `sync` |
| ------- | ------ |
| Returns | `void` |

Autofilling the input on the <kbd>tab</kbd> press is set using this command. It is reset automatically after calling prompt.

### Example:

This example auto-fills the input with "hello" when <kbd>tab</kbd> is pressed and the input is "h":

```js
async function main() {
  read.setAutofill(input => (input === "h")? "ello" : ""))
  let input = read.prompt("Length of: ")
  console.log("Length of " + input + " is " + input.length + ".")
}
main()

```

The application automatically calls the function when the input changes with one argument, the input at the time. The return value of the function is then **appended** to the input. As in the above example, the `input` is "h" and the return-value of the autofill function is "ello", which is appended to the input, "h" + "ello" = "hello".

### `function`

| Type     | `string` |
| -------- | -------- |
| Optional | `false`  |

The function which returns text for autofilling.
