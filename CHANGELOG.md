# v1.0.4

- Fixed double new-line print.

# v1.0.3

- Fixed #2
- Newline printed after prompt.

# v1.0.2

- First github release.
- Added basic commands.
